import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.CartDetails();
    this.loadCart();
  }

  getCartDetails: any = [];
  CartDetails() {
    if (localStorage.getItem('localCart')) {
      this.getCartDetails = JSON.parse(localStorage.getItem('localCart') || '[]');
      console.log(this.getCartDetails);
    }
  }

  increaseQnt(productId: any, quantity: any) {
    for (let i = 0; i < this.getCartDetails.length; i++) {
      if (this.getCartDetails[i].productId === productId) {
        if (quantity != 5) {
          this.getCartDetails[i].quantity = parseInt(quantity) + 1;
        }
      }
    }
    localStorage.setItem('localCart', JSON.stringify(this.getCartDetails));
    this.loadCart();
  }

  decreaseQnt(productId: any, quantity: any) {
    for (let i = 0; i < this.getCartDetails.length; i++) {
      if (this.getCartDetails[i].productId === productId) {
        if (quantity != 1) {
          this.getCartDetails[i].quantity = parseInt(quantity) - 1;
        }
      }
    }
    localStorage.setItem('localCart', JSON.stringify(this.getCartDetails));
    this.loadCart();
  }

  total: number = 0;
  loadCart() {
    if (localStorage.getItem('localCart')) {
      this.getCartDetails = JSON.parse(localStorage.getItem('localCart') || '[]');
      this.total = this.getCartDetails.reduce(function (acc: any, val: any) {
        return acc + (val.amt * val.quantity)
      }, 0)
    }
  }

  removeAll() {
    localStorage.removeItem('localCart');
    this.getCartDetails = [];
    this.total = 0;
    this.cartNumber = 0;
    this.auth.cartSubject.next(this.cartNumber);
  }

  singleDelete(getCartDetail:any) {
    console.log(getCartDetail);
    if(localStorage.getItem('localCart')){
      this.getCartDetails = JSON.parse(localStorage.getItem('localCart') || '[]');
      for(let i = 0; i < this.getCartDetails.length; i++){
        if(this.getCartDetails[i].productId === getCartDetail){
          this.getCartDetails.splice(i, 1);
          localStorage.setItem('localCart',JSON.stringify(this.getCartDetails));
          this.loadCart();
          this.cartNumberFun();
        }
      }
    }
  }

  cartNumber:number = 0;
  cartNumberFun(){
    var cartValue = JSON.parse(localStorage.getItem('localCart') || '{}'); 
    this.cartNumber = cartValue.length;
    this.auth.cartSubject.next(this.cartNumber);
  }

}
