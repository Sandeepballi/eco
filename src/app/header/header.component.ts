import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: AuthService) {
    // setInterval(this.cartItemFun, 100)
    this.auth.cartSubject.subscribe((data) => {
      this.cartItem = data;
    })
  }

  ngOnInit(): void {
    this.cartItemFun();
  }
  cartItem:number = 0;
  cartItemFun(){
    if(localStorage.getItem('localCart') != null){
      var cartCount = JSON.parse(localStorage.getItem('localCart') || '[]');
    
      // console.log(cartCount);
      this.cartItem = cartCount.length;
    }
  }
}



// cartItem: number = 0;
// cartItemFun() {
//   if (localStorage.getItem('localCart')) {
//     // let cartCount:any = 0;
//     let localData: any = localStorage.getItem('localCart' || '[]')
//     let cartCount = localData.length;
//     this.cartItem = cartCount;
//     console.log(this.cartItem);
//     console.log(localData.length);
//     console.log(localData.lastIndex);
//     console.log(cartCount);
//   }
// }
