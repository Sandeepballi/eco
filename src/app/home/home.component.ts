import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  productArray = [
    {
      productId: 1,
      name: "Milk",
      img: "../../assets/milk.jfif",
      amt: 125,
      quantity: 1
    },
    {
      productId: 2,
      name: "Butter",
      img: "../../assets/butter.jpg",
      amt: 200,
      quantity: 1
    },
    {
      productId: 3,
      name: "Vegetables",
      img: "../../assets/vegetables.jfif",
      amt: 300,
      quantity: 1
    },
    {
      productId: 4,
      name: "Panner",
      img: "../../assets/paneer.jpg",
      amt: 250,
      quantity: 1
    },
  ];

  increase(products: any) {
    if (products.quantity != 5) {
      products.quantity += 1;
    } else if (products.quantity == 5) {
      alert('Limit Exceeded')
    }
  }

  decrease(products: any) {
    if (products.quantity != 1) {
      products.quantity -= 1;
    }
  }

  itemsCart: any = [];
  addCart = (category: any) => {
    // console.log(category);
    let cartDataNull = localStorage.getItem('localCart');
    if (cartDataNull === null) {
      let getStoreData: any = [];
      getStoreData.push(category);
      localStorage.setItem('localCart', JSON.stringify(getStoreData));
    } else {
      let id = category.productId;
      let index: number = -1;
      this.itemsCart = JSON.parse(localStorage.getItem('localCart') || '[]');
      let tempData = JSON.parse(localStorage.getItem('localCart') || '[]');
      
      let refreshData = tempData.map((el:any, i:number) => {
        if(el.productId === category.productId){
          index = i;
          return category;
        }
        return el;
      })
      // for (let i = 0; i < this.itemsCart.length; i++) {
      //   if (parseInt(id) === parseInt(this.itemsCart[i].productId)) {
      //     this.itemsCart[i].quantity = category.quantity;
      //     index = i;
      //     break;
      //   }
      // }
      if (index === -1) {
        // this.itemsCart.push(category);
        refreshData.push(category);
        this.itemsCart = refreshData;
        localStorage.setItem('localCart', JSON.stringify(this.itemsCart))
      }
      else {
        this.itemsCart = refreshData;
        localStorage.setItem('localCart', JSON.stringify(this.itemsCart))
      }
    }
    this.cartNumberFun();
  }

  cartNumber:number = 0;
  cartNumberFun(){
    var cartValue = JSON.parse(localStorage.getItem('localCart') || '{}');
    this.cartNumber = cartValue.length;
    this.auth.cartSubject.next(this.cartNumber);
  }
}



